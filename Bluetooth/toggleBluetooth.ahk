﻿#NoEnv 
#Persistent
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
!b::
psScript := ".\script.ps1"
RunWait, powershell.exe "%psScript%"
return